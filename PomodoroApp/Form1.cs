﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PomodoroApp {
    public partial class Form1 : Form {
        Pomodoro pomodoro;

        public Form1() {
            pomodoro = new Pomodoro();
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, EventArgs e) {
            tmrClock.Enabled = !tmrClock.Enabled;
        }

        private void tmrClock_Tick(object sender, EventArgs e) {
            pomodoro.Tick();
            UpdateStatus();
        }

        private void btnReset_Click(object sender, EventArgs e) {
            tmrClock.Enabled = false;

            try {
                var minutesWork = int.Parse(tbWork.Text);
                var minutesRest = int.Parse(tbRest.Text);
                pomodoro = new Pomodoro(minutesWork, minutesRest);
            } catch (Exception) {
                MessageBox.Show("Upišite ispravnu vrijednost!");
            }
            UpdateStatus();
        }

        // UpdateStatus ...........
        private void UpdateStatus() {
            lblStatus.Text = pomodoro.ToString();
        }
    }
}
